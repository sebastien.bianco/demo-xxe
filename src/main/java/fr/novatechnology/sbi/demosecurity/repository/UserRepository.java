package fr.novatechnology.sbi.demosecurity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.novatechnology.sbi.demosecurity.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	public List<User> findByFirstName(String firstName);
	
	public List<User> findByLastName(String lastName);
	
}