package fr.novatechnology.sbi.demosecurity.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import fr.novatechnology.sbi.demosecurity.model.SearchCriteria;
import fr.novatechnology.sbi.demosecurity.model.User;
import fr.novatechnology.sbi.demosecurity.repository.UserRepository;

@RestController
@RequestMapping("/api/v1")
public class UserController {
	@Autowired
	private UserRepository userRepository;
	
	
	/**
	 * Get all users list.
	 *
	 * @return the list
	 */
	@GetMapping("/users")
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	/**
	 * Gets users by id.
	 *
	 * @param userId the user id
	 * @return the users by id
	 * @throws UserNotFoundException the resource not found exception
	 */
	@GetMapping("/users/{id}")
	public ResponseEntity<User> getUsersById(@PathVariable(value = "id") Long userId) {
		User user = userRepository.findById(userId).orElseThrow(
				() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found on ::: " + userId, null));
		return ResponseEntity.ok().body(user);
	}

	/**
	 * Create user user.
	 *
	 * @param user the user
	 * @return the user
	 */
	@PostMapping("/users")
	@Transactional
	@ResponseStatus(code = HttpStatus.CREATED)
	public User createUser(@Valid @RequestBody User user) {
		return userRepository.save(user);
	}

	/**
	 * Update user response entity.
	 *
	 * @param userId      the user id
	 * @param userDetails the user details
	 * @return the response entity
	 * @throws UserNotFoundException the resource not found exception
	 */
	@PutMapping("/users/{id}")
	@Transactional
	public ResponseEntity<User> updateUser(@PathVariable(value = "id") Long userId,
			@Valid @RequestBody User userDetails) {
		User user = userRepository.findById(userId).orElseThrow(
				() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found on ::: " + userId, null));
		user.setEmail(userDetails.getEmail());
		user.setLastName(userDetails.getLastName());
		user.setFirstName(userDetails.getFirstName());
		user.setUpdatedAt(new Date());
		final User updatedUser = userRepository.save(user);
		return ResponseEntity.ok(updatedUser);
	}

	/**
	 * Delete user map.
	 *
	 * @param userId the user id
	 * @return the map
	 * @throws Exception the exception
	 */
	@DeleteMapping("/user/{id}")
	public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long userId) {
		User user = userRepository.findById(userId).orElseThrow(
				() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found on ::: " + userId, null));
		userRepository.delete(user);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
	
	@PostMapping("/users/search")
	public List<User> searchUser(@Valid @RequestBody String xml){
		System.out.println(xml);
		XmlMapper xmlMapper = new XmlMapper();
	    try {
			SearchCriteria searchCriteria = xmlMapper.readValue(xml, SearchCriteria.class);
			System.out.println(searchCriteria);
			if(searchCriteria.getFirstName() != null && !searchCriteria.getFirstName().isBlank()) {
				return returnListOrThrowException(userRepository.findByFirstName(searchCriteria.getFirstName()),searchCriteria);
			} else if(searchCriteria.getLastName() != null && !searchCriteria.getLastName().isBlank()) {
				return returnListOrThrowException(userRepository.findByLastName(searchCriteria.getLastName()),searchCriteria);
			} else {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid criteria : " + searchCriteria, null);
			}
		} catch (JsonMappingException e) {
			System.err.println(e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "JsonMappingException ::: " + e.getMessage(), e);
		} catch (JsonProcessingException e) {
			System.err.println(e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "JsonProcessingException ::: " + e.getMessage(), e);
		} catch (IOException e) {
			System.err.println(e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "IOException ::: " + e.getMessage(), e);
		
		}
		
	}

	private List<User> returnListOrThrowException(List<User> resultList, SearchCriteria criteria) {
		if (resultList != null && !resultList.isEmpty()) {
			return resultList;			
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No users found with criteria : " + criteria, null);			
		}
	}
}