package fr.novatechnology.sbi.demosecurity.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Data;

@Data
@JacksonXmlRootElement(localName = "search")
public class SearchCriteria {

	private String firstName;
	private String lastName;
}
