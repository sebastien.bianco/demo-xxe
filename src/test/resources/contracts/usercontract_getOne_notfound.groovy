package userservice

import org.springframework.cloud.contract.spec.Contract

Contract.make {
  description("Get a User by Id (not found expected)")
  request {
    method 'GET'
    url '/api/v1/users/2'
    headers {
      contentType(applicationJson())
    }
  }
  response {
    status NOT_FOUND()    
  }
}