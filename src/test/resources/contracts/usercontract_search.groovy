package userservice

import org.springframework.cloud.contract.spec.Contract

Contract.make {
  description("When a POST request on search api is sent, the user are looked for according to criterias")
  request {
    method 'POST'
    url '/api/v1/users/search'
    body(
      "<search><firstName>seb</firstName></search>"
    )
    headers {
      contentType(applicationJson())
    }
  }
  response {
    status OK()
    body([])//required for activate body Matcher 
	bodyMatchers {
		// asserts the jsonpath value against manual regex
		jsonPath('$.[*].id', byRegex(number()))
		jsonPath('$.[*].firstName', byRegex(onlyAlphaUnicode()))
		jsonPath('$.[*].lastName', byRegex(onlyAlphaUnicode()))
		jsonPath('$.[*].email', byRegex(nonEmpty()))
		jsonPath('$.[*].email', byRegex(email()))
	}
    headers {
      contentType(applicationJson())
    }
  }
}