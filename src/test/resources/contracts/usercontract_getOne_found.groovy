package userservice

import org.springframework.cloud.contract.spec.Contract

Contract.make {
  description("Get a User by Id")
  request {
    method 'GET'
    url '/api/v1/users/1'
    headers {
      contentType(applicationJson())
    }
  }
  response {
    status 200
    body(
      id: 1,
	  firstName: "Bob",
	  lastName: "Sinclar"
    )
    headers {
      contentType(applicationJson())
    }
  }
}