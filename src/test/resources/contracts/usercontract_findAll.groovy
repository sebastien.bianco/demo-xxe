package userservice

import java.io.ObjectInputFilter.Status

import org.springframework.cloud.contract.spec.Contract

Contract.make {
  description("When a POST request with a User is made, the created user's ID is returned")
  request {
	method 'GET'
	url '/api/v1/users'
	headers {
	  contentType(applicationJson())
	}
  }
  response {
	status OK()
	body([])//required for activate body Matcher 
	bodyMatchers {
		// asserts the jsonpath value against manual regex
		jsonPath('$.[*].id', byRegex(number()))
		jsonPath('$.[*].firstName', byRegex(onlyAlphaUnicode()))
		jsonPath('$.[*].lastName', byRegex(onlyAlphaUnicode()))
		jsonPath('$.[*].email', byRegex(nonEmpty()))
		jsonPath('$.[*].email', byRegex(email()))
	}
	headers {
	  contentType(applicationJson())
	}
  }
}