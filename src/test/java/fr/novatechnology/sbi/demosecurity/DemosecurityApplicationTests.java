package fr.novatechnology.sbi.demosecurity;

import java.util.Arrays;
import java.util.Optional;

import org.assertj.core.internal.Lists;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import fr.novatechnology.sbi.demosecurity.model.User;
import fr.novatechnology.sbi.demosecurity.repository.UserRepository;
import io.restassured.module.mockmvc.RestAssuredMockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@DirtiesContext
@AutoConfigureMessageVerifier
public abstract class DemosecurityApplicationTests {

	@Autowired
	WebApplicationContext webApplicationContext;

	@MockBean
	private UserRepository userRepository;

	@Before
	public void setup() {
		User savedUser = createUserObject(42L, "Arthur", "Dent");
		User unicUser = createUserObject(1L, "Bob", "Sinclar");

		// Save
		Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(savedUser);

		// Find By Id
		Mockito.when(userRepository.findById(1L)).thenReturn(Optional.of(unicUser));
		Mockito.when(userRepository.findById(2L)).thenReturn(Optional.empty());

		// Find All
		Mockito.when(userRepository.findAll()).thenReturn(
				Arrays.asList(createUserObject(1L, "Bob", "Sinclar"), createUserObject(2L, "Jean", "Pierrot")));

		// Find By First Name
		Mockito.when(userRepository.findByFirstName(Mockito.anyString())).thenReturn(
				Arrays.asList(createUserObject(1L, "Bob", "Sinclar"), createUserObject(2L, "Bob", "Pierrot")));

		// Find By Last Name
		Mockito.when(userRepository.findByLastName(Mockito.anyString())).thenReturn(
						Arrays.asList(createUserObject(1L, "Bob", "Sinclar"), createUserObject(2L, "Jean", "Sinclar")));

		
		RestAssuredMockMvc.webAppContextSetup(webApplicationContext);
	}

	private static User createUserObject(Long id, String firstName, String lastName) {
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setId(id);
		user.setEmail(firstName + "." + lastName + "@provider.cool");
		return user;
	}
}