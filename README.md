# demo-xxe

A sample projet to demonstrate XXE Security Vulnerability (https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Processing)

## Step 1 : 

* XXE Vulnerability available. NB : Test are non functionnal due to downgrad of jackson library.


## Step 2 :

* External entities disabled. NB : Test are non functionnal due to downgrad of jackson library.

## Step 3 :

* Remove dedicated configuration and use of last library version (Test Spring Cloud Contract OK)